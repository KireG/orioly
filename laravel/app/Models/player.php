<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class player extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'surname',
        'number',
        'team_id',
        'position_id'
    ];

    public function teams(){
        return $this->belongsTo(player::class);
    }

    public function positions(){
        return $this->belongsTo(position::class);
    }

}
