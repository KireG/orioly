<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class team extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'link',
        'country_id',
    ];

    public function players(){
        return $this->hasMany(player::class);
    }

    public function countries(){
        return $this->belongsTpo(country::class);
    }

}
