<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('players')->insert([
            ['name' => 'John','surname'=>'Doe','number'=>1,'team_id'=>1,'position_id'=>1],
            ['name' => 'Jane','surname'=>'Doe','number'=>12,'team_id'=>2,'position_id'=>2],
            ['name' => 'John','surname'=>'Doe','number'=>4,'team_id'=>3,'position_id'=>3],
            ['name' => 'John','surname'=>'Doe','number'=>15,'team_id'=>4,'position_id'=>4],
            ['name' => 'John','surname'=>'Doe','number'=>8,'team_id'=>5,'position_id'=>5],
            ['name' => 'John','surname'=>'Doe','number'=>24,'team_id'=>1,'position_id'=>1],
            ['name' => 'John','surname'=>'Doe','number'=>2,'team_id'=>2,'position_id'=>2],
            ['name' => 'John','surname'=>'Doe','number'=>10,'team_id'=>3,'position_id'=>3],
            ['name' => 'John','surname'=>'Doe','number'=>9,'team_id'=>4,'position_id'=>4],
        ]);
    }
}
