<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            ['name' => 'Center'],
            ['name' => 'Visoko krilo'],
            ['name' => 'Nisko krilo'],
            ['name' => 'Bek suter'],
            ['name' => 'Playmaner'],
        ]);
    }
}
