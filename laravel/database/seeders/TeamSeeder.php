<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            ['name' => 'Cibona','link'=>'lin','country_id'=>1],
            ['name' => 'MZT','link'=>'lin','country_id'=>12],
            ['name' => 'Orlando','link'=>'lin','country_id'=>13],
            ['name' => 'ABC','link'=>'lin','country_id'=>14],
            ['name' => 'Beneton','link'=>'lin','country_id'=>15],
        ]);

    }
}
